﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

	public int m_health = 10;

	float originalY;
	public float floatStrength = 0.25f; 

	// Use this for initialization
	void Start () {
		this.originalY = this.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(transform.position.x,
			originalY + ((float)System.Math.Sin(Time.time) * floatStrength),
			transform.position.z);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			Player player = coll.gameObject.GetComponent<Player> ();
			player.incrementHealth(m_health);
			Destroy (this.gameObject);
		}

	}

}


