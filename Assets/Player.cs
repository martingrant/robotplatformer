﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	public float m_jumpForce = 400.0f;
	public float m_maxSpeed = 5.0f;
	public Text m_healthText;
	public Text m_gameOverText;
	public int m_health = 50;
	public int m_meleeDamage = 10;

	private Rigidbody2D m_rigidBody2D;
	private bool m_doubleJumped = false;
	private bool m_grounded = true;
	private int m_direction = 1;

	public GameObject m_projectile;
	private GameObject m_projectileInstance;


	// Use this for initialization
	void Start () {
		m_rigidBody2D = GetComponent<Rigidbody2D>();

		m_healthText.text = "HP: " + m_health;
		m_gameOverText.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

		float move = Input.GetAxis("Horizontal");

		if (Input.GetButtonDown ("Left") == true) {
			m_direction = -1;
		} 
		else if (Input.GetButtonDown ("Right") == true) {
			m_direction = 1;
		}

		m_rigidBody2D.velocity = new Vector2(move * m_maxSpeed, m_rigidBody2D.velocity.y);

		if (Input.GetButtonDown ("Jump") == true) {
			if (m_grounded || !m_doubleJumped) {
				m_rigidBody2D.AddForce(new Vector2 (0f, m_jumpForce));
				m_doubleJumped = !m_grounded;
			}
		} else if (m_grounded) {
			m_doubleJumped = false;
		}

		if (m_rigidBody2D.velocity.y == 0)
			m_grounded = true;
		else
			m_grounded = false;


		if (Input.GetButtonDown ("Fire1") == true) {
			meleeAttack ();
		}

		if (Input.GetButtonDown ("Fire2") == true) {
			if (m_direction == -1) {
				Vector3 position = new Vector3 (gameObject.transform.position.x - 2, gameObject.transform.position.y, gameObject.transform.position.z);
				m_projectileInstance = Instantiate (m_projectile, position, Quaternion.Euler(Vector3.down * 180f)) as GameObject;
			} else {
				Vector3 position = new Vector3 (gameObject.transform.position.x + 2, gameObject.transform.position.y, gameObject.transform.position.z);
				m_projectileInstance = Instantiate (m_projectile, position, Quaternion.identity) as GameObject;
			}
		}


	}

	void updateHUD() {
		m_healthText.text = "HP: " + m_health;
	}

	public void incrementHealth(int amount) {
		m_health += amount;

		if (m_health < 1) {
			m_health = 0;
			m_gameOverText.enabled = true;
			StartCoroutine (restart (5));
		}

		if (m_health > 100)
			m_health = 100;

		updateHUD();
	}
		

	void meleeAttack() {
		// Use raycast to check if Enemy is in front of Player, if so then damage it
		Vector2 dir = new Vector2(m_direction,0);
		Vector2 start = transform.position;
		start += dir;

		RaycastHit2D ray = Physics2D.Raycast(start, dir, 1);
		if (ray && ray.collider.gameObject.tag == "Enemy") {
			Enemy enemy = ray.collider.gameObject.GetComponent<Enemy>();
			enemy.takeDamage (m_meleeDamage);
		}
	}


	IEnumerator restart(float time)
	{
		yield return new WaitForSeconds(time);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
		
}