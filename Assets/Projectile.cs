﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float m_speed;
	public int m_damage;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.position += gameObject.transform.right * m_speed;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Enemy") {
			Enemy enemy = coll.gameObject.GetComponent<Enemy>();
			enemy.takeDamage (m_damage);
			Destroy (gameObject);
		}
	}

	void OnBecameInvisible() {
		Destroy(gameObject);
	}
}
