﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public float m_speed = 1.0f;
	public int m_sightRange = 4;
	public int m_damage = 10;
	public int m_health = 50;

	private Rigidbody2D m_rigidBody2D;
	private int m_direction = 1;

	private GameObject m_target;

	// Use this for initialization
	void Start () {
		m_rigidBody2D = GetComponent<Rigidbody2D>();

	}


	// Update is called once per frame
	void Update () {

		// Raycast for edge of platform and flip direction
		checkForPlatformEdge ();

		// Update movement normally unless the player is within range, chase player
		if (!checkForPlayer ())
			m_rigidBody2D.velocity = new Vector2(m_direction * m_speed, m_rigidBody2D.velocity.y);

	}


	// Sends a raycast next to and down from the player to check for empty space
	void checkForPlatformEdge() {
		Vector3 checkRight = new Vector2(m_direction, 0);
		Vector3 start = transform.position;

		Vector3 fwd = transform.TransformDirection(Vector3.down);

		start += checkRight;

		if (!Physics2D.Raycast (start, fwd, 3))
			m_direction = -m_direction;
	}


	// Raycast horizontally from the player to check if the player can be seen, move towards if so
	bool checkForPlayer() {
		Vector2 dir = new Vector2(m_direction,0);
		Vector2 start = transform.position;
		start += dir;

		RaycastHit2D ray = Physics2D.Raycast(start, dir, m_sightRange);
		if (ray && ray.collider.gameObject.tag == "Player") {
			m_target = ray.collider.gameObject;
			transform.position = Vector2.MoveTowards (transform.position, m_target.transform.position, m_speed * Time.deltaTime);

			return true;
		}

		return false;
	}


	public void takeDamage(int amount) {
		// Decrement health and push enemy when attacked, destroy if no health left
		m_health -= amount;

		m_rigidBody2D.AddForce(new Vector2(-m_direction * 300, 0));
		m_rigidBody2D.AddForce(m_rigidBody2D.transform.up * 200);
	
		if (m_health < 1)
			Destroy (this.gameObject);
	}


	void OnCollisionEnter2D(Collision2D coll) {
		// Swap direction if the enemy hits a tile
		if (coll.gameObject.tag == "Tiles") {
			m_direction = -m_direction;
		}

		// If collision with Player, knock both Enemy and Player back and damage Player
		if (coll.gameObject.tag == "Player") {
			Rigidbody2D body = coll.gameObject.GetComponent<Rigidbody2D> ();

			body.AddForce(new Vector2(m_direction * 2000, 0));
			body.AddForce(body.transform.up * 500);

			Player player = coll.gameObject.GetComponent<Player> ();
			player.incrementHealth (-5);
		}
	}


}
